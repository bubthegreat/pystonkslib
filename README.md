# pystonkslib

Stonks are cool - and if you want to do your own analysis, you have to pull all the data.

## Installation

```bash
pip install pystonkslib
```

```
(venv) PS C:\Users\bub\Development\pystonkslib> pslib --help
usage: usage: pslib [-h] [-g] [-c]

optional arguments:
  -h, --help            show this help message and exit
  -g, --get-symbols     Get updated stock symbols.
  -c, --get-chunked-data
                        Get chunked data.

optional arguments:
  -h, --help            show this help message and exit
  -g, --get-symbols     Get updated stock symbols.
  -c, --get-chunked-data
                        Get chunked data.
```
