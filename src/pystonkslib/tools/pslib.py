"""Util to update ticker definitions."""
import asyncio
import typer
from pystonkslib.tickers import get_chunked_data, get_stock_symbols_file, get_serial_data

app = typer.Typer()

@app.command()
def get_data(serial: bool = True):
    if serial:
        get_serial_data()
    else:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(get_chunked_data())

@app.command()
def get_symbols(update: bool = True):
    get_stock_symbols_file()

def main() -> None:
    """Run main symbol update logic."""
    app()

