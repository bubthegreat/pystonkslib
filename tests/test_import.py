"""General import tests."""


def test_import() -> None:
    """Test that we can import the top level package."""
    import pystonkslib  # pylint: disable=unused-import,import-outside-toplevel
